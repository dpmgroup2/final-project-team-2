package Settings;
/**
 * Contains all Robots Settings
 * @author Bassam Riman
 *
 */
public final class RobotSettings {
		public static final boolean displayON = false;
		public static final int stepsBeforeLoc = 6;
		public static final int howManyShots = 5;//max is 5, min is 1
		public static final int ballDelay = 45000;//45000;//5 balls, care!!!!
}
