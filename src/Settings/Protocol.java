package Settings;
/**
 * Contains index of object in arrays, this class helps make it easier for humain to read
 * and remember where and object is stored in an array
 * @author Bassam Riman
 *
 */
public final class Protocol {
	//RobotController: BT data*************************************************
		public static final byte role = 0;
		public static final byte bx = 1;
		public static final byte by = 2;
		public static final byte w1 = 3;
		public static final byte w2 = 4;
		public static final byte d1 = 5;
		public static final byte startingCorner = 6;
	//RobotController: shootSpots*************************************************
		public static final byte shootXft = 0;
		public static final byte shootYft = 1;
		public static final byte shootYclose = 2;
		public static final byte shootTheta = 3;
	//Input: dataCollected array***********************************************
		public static final byte LightSensorsDATA = 0;
				public static final byte lLightS = 0;
				public static final byte rLightS = 1;
		public static final byte TachoCountSensorsDATA = 1;
				public static final byte rWheel = 0;
				public static final byte lWheel = 1;
		public static final byte UltrasonicSensorDATA = 2;
				public static final byte fUltrasonicS = 0;
				public static final byte lsUltrasonicS = 1;
				public static final byte rsUltrasonicS = 2;
		public static final byte TouchSensorDATA = 3;
				public static final byte fTouchS = 0;
	//InputProcessor: DataProcessed array*********************************
		public static final byte odometerInputProcess = 0;
				public static final byte X = 0;
				public static final byte Y = 1;
				public static final byte Theta = 2;
		public static final byte localizationInputProcess = 1;
				public static final byte fUSout=0;
				public static final byte rLSout=1;
		public static final byte driverInputProcess=2;
				public static final byte rUS = 0;
				public static final byte lUS = 1;
				public static final byte fUS = 2;
				public static final byte rLS = 3;
				public static final byte lLS = 4;
				public static final byte fTS = 5;
}
