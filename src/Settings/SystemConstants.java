package Settings;
/**
 * Holds all System Constants and array protocol
 * @author Bassam Riman
 *
 */
public final class SystemConstants {
	//Robot Constant**************************************************************
			public static final float lWheel=(float) 2.4641;//2.4641 on the line, slight left//2.4594kinda straight left over, right under;//2.4493//2.55//2.465 still drifting left, too much//2.472
			public static final float rWheel=(float) 2.5294;//2.5257 overshoot//2.5283;//2.5228 drift right tiny, overshoot//2.5883 still left. too little//2.49
			public static final float width=(float) 18.0548;//.1022 18.0548 17.9056 17.9802
			public static final float foot = (float) 30.48;
			public static final int acceleration = 500;
	//Localization constants******************************************************
			public static final int usLocThresh=40;
			public static final float lsOffset=(float) 11.85;
			public static final int angleNailing=25;
			public static final float angleOffset=(float)239;//239 is alligned//236 almost goals
	//Emergency constants*********************************************************
			public static final int usFrontThresh=13;
			public static final int usSideThresh=10;
	//Timer periods***************************************************************
		//Input
			public static final byte InputPERIOD = 50;		
}