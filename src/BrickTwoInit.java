import lejos.nxt.Button;
import BrickCommunication.B2toB1Connection;

/**
 * 
 * Initialize all the system in brick two, first class to be called (contains the main)
 * @author Bassam Riman
 *
 */
public class BrickTwoInit {
	public static void main(String[] args) {
		B2toB1Connection brickOneConnection = new B2toB1Connection();
				
		brickOneConnection.start();
		Button.waitForAnyPress();

		System.exit(0);
	}
}
