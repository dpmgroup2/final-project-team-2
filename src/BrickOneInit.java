  
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.util.Delay;
import BrickOne.PrimaryController.RobotController;

/**
 * 
 * Initialize all the system in brick one, first class to be called (contains the main)
 * @author Bassam Riman
 *
 */
public class BrickOneInit {
	public static void main(String[] args) { 
				
		Button.waitForAnyPress();
		Motor.A.resetTachoCount();
		Motor.B.resetTachoCount();
		
		Delay.msDelay(2000);
		new RobotController();

		Button.waitForAnyPress();
		System.exit(0);
	}
}