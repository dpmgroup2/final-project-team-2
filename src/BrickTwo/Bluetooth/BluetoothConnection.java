package BrickTwo.Bluetooth;

import java.io.DataInputStream;
import java.io.IOException;

import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;


public class BluetoothConnection {
	private Transmission trans;
	
	public BluetoothConnection() {
		NXTConnection conn = Bluetooth.waitForConnection();
		DataInputStream diss = conn.openDataInputStream();
		this.trans = ParseTransmission.parse(diss);
		try {
			diss.close();
		} catch (IOException e) {}
		conn.close();
	}
	
	public Transmission getTransmission() {
		return this.trans;
	}
}
