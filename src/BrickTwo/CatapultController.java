package BrickTwo;

import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;

public class CatapultController extends Thread{
	
	NXTRegulatedMotor leftMotor = Motor.A, rightMotor = Motor.B, defenceMotor=Motor.C;
	private int x;
	private boolean shoot=false;

	@Override
	public void run() {

		defenceMotor.stop();
		leftMotor.setSpeed(100);
		rightMotor.setSpeed(100);
		leftMotor.rotate(-15, true);
		rightMotor.rotate(-15, true);
		try { Thread.sleep(1000); } catch(Exception e){}
		leftMotor.flt();
		rightMotor.flt();
		try { Thread.sleep(1000); } catch(Exception e){}
		leftMotor.resetTachoCount();
		rightMotor.resetTachoCount();
		
		while(true) {
			if(shoot){
				leftMotor.setAcceleration(9999999);
				rightMotor.setAcceleration(9999999);
				leftMotor.setSpeed(200);
				rightMotor.setSpeed(200);
				leftMotor.rotate(140, true);
				rightMotor.rotate(140, true);
				
				while(leftMotor.isMoving()||rightMotor.isMoving()) {
					try { Thread.sleep(500); } catch(Exception e){}
				}
				
				//going up slightly

				leftMotor.setAcceleration(100);
				rightMotor.setAcceleration(100);
				leftMotor.setSpeed(100);
				rightMotor.setSpeed(100);
				leftMotor.rotate(-50, true);
				rightMotor.rotate(-50, true);
				try { Thread.sleep(2000); } catch(Exception e){}
				
				
				//SHOoooTTTT!!!
				leftMotor.setAcceleration(9999999);
				rightMotor.setAcceleration(9999999);
				leftMotor.setSpeed(getSpeed(x));
				rightMotor.setSpeed(getSpeed(x));
				leftMotor.rotate(-getAngle(x), true);
				rightMotor.rotate(-getAngle(x), true);
				
				try { Thread.sleep(2000); } catch(Exception e){}
	

				leftMotor.setAcceleration(200);
				rightMotor.setAcceleration(200);
				leftMotor.setSpeed(200);
				rightMotor.setSpeed(200);
				leftMotor.rotate(getAngle(x)-90, true);
				rightMotor.rotate(getAngle(x)-90, true);

				try { Thread.sleep(1000); } catch(Exception e){}
				leftMotor.flt();
				rightMotor.flt();
				try { Thread.sleep(1000); } catch(Exception e){}
				leftMotor.resetTachoCount();
				rightMotor.resetTachoCount();
				
				try { Thread.sleep(2000); } catch(Exception e){}
				shoot=false;
			}
			else{
				try { Thread.sleep(2000); } catch(Exception e){}
			}
		}
	}
	
	
	public static int getSpeed(int distance){
		
		switch (distance){
			case 7:
				return 285;
			case 8:
				return 280;
			case 9:
				return 290;
		}
		return 0;
	}

	public static int getAngle(int distance){
		
		switch (distance){
			case 7:
				return 60;
			case 8: 
				return 70;
			case 9:
				return 90;
		}
		return 0;
	}
	

	public void setDistance(int x) {
		this.x = x;
	}

	public void setShoot(boolean shoot) {
		this.shoot = shoot;
	}
	
	public boolean getShoot() {
		return shoot;
	}
	
	public void deployDef(){
		defenceMotor.setAcceleration(99999);
		defenceMotor.setSpeed(400);
		defenceMotor.resetTachoCount();
		defenceMotor.rotate(-20);
		while(Math.abs(defenceMotor.getTachoCount())<20){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		defenceMotor.flt();
		System.exit(0);
	}
}
