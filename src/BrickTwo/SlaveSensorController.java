package BrickTwo;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;

/**
 * Class that poll the ultrasonic sensor on brick two and updates its instance, getters are
 * called when a poll request has been sent from brick one
 * @author Bassam Riman
 *
 */
public class SlaveSensorController {
	
	private UltrasonicSensor leftSideSonic = new UltrasonicSensor(SensorPort.S1);
	private UltrasonicSensor rightSideSonic = new UltrasonicSensor(SensorPort.S3);
	TouchSensor touch0 = new TouchSensor(SensorPort.S2);
	
	private final int FILTER_OUT = 5, FILTER_THRESH=15; //number of no echo errors to filter out
	private int[] leftSonicA = new int[4];
	private int[] rightSonicA = new int[4];
	private boolean usEnabled=true;
	
	/**
	 * Class Constructor
	 */
	public SlaveSensorController(){
		for(int i=0; i<3; i++) {
			leftSonicA[i]=0;
			rightSonicA[i]=0;
		}
		leftSonicA[3]=500;
		rightSonicA[3]=500;
		
		leftSideSonic.continuous();
		rightSideSonic.continuous();
	}
	

	public void disableUS(){
		usEnabled=false;
		leftSideSonic.off();
		rightSideSonic.off();
		leftSonicA[2]=70;
		rightSonicA[2]=70;
	}
	
	public void enableUS(){
		usEnabled=true;
		for(int i=0; i<3; i++) {
			leftSonicA[i]=0;
			rightSonicA[i]=0;
		}
		leftSonicA[3]=500;
		rightSonicA[3]=500;
		leftSideSonic.continuous();
		rightSideSonic.continuous();
	}
	
	/**
	 * Getter
	 * @return
	 */
	public byte getTouch(){
		if(touch0.isPressed()) return 1;
		else return 0;
	}
	
	/**
	 * Getter
	 * @return
	 */
	public byte getRSonicDistance(){
		if(usEnabled) filterData(rightSideSonic, rightSonicA);
		return (byte) rightSonicA[2];
	}
	/**
	 * Getter
	 * @return
	 */
	public byte getLSonicDistance(){
		if(usEnabled) filterData(leftSideSonic, leftSonicA);
		return (byte) leftSonicA[2];
	}
	
	private void filterData(UltrasonicSensor us, int[] sonicA){
		//Poll the sensor and get the distance
		int distance = us.getDistance();
		if (distance > 70 && sonicA[0] < FILTER_OUT) {
			//If it's an error and the error limit is not met,
			//asume there's a gap and tell the controller to go straight
			//and increment the counter
			sonicA[0] ++;
		}
		else if (distance>70){
			//Once the error limit is reached, consider this as a true 255 
			sonicA[2]=70;
			sonicA[1]=0;
		}
		else {
			//The sensor has picked up the wall once again, reset filter
			sonicA[0] = 0;
			if( (Math.abs(distance-sonicA[2])>FILTER_THRESH || Math.abs(distance-sonicA[3])>FILTER_THRESH )  && sonicA[1] <FILTER_OUT) {
				sonicA[3]=distance;
				sonicA[1]++;
				
			}
			else {
				sonicA[2]=distance;
				sonicA[3]=500;
				sonicA[1]=0;
			}
		}
	}
}
