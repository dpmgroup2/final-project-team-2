package BrickOne.SecondaryController;

import java.util.LinkedList;

/**
 * This is an implementation of the A* search algorithm. Typically this object would be instantiated and then used
 * in a WayPointPathFinder constructor, along with a set of connected nodes.
 * @see lejos.robotics.pathfinding.WayPointPathFinder
 * @author BB
 * 
 */
public class Astar {
	
	public static LinkedList<WayPoint> findPath(WayPoint start, WayPoint goal) {
		
		LinkedList <WayPoint> closedset = new LinkedList<WayPoint>(); // The set of nodes already evaluated. Empty at start.     
		LinkedList <WayPoint> openset = new LinkedList<WayPoint>(); // The set of tentative nodes to be evaluated. 
		openset.add(start);
		start.setG_Score(0);
		start.setH_Score(start.getDistance(goal));
		
		while (!openset.isEmpty()) {
			WayPoint x = getLowestCost(openset); // get the node in openset having the lowest f_score[] value

			
			if(x == goal) {
				LinkedList<WayPoint> final_path = new LinkedList<WayPoint>();
				
				reconstructPath(goal, start, final_path);
				return final_path;
			}
			openset.remove(x); // remove x from openset
			closedset.add(x); // add x to closedset

			LinkedList <WayPoint> xLinks = x.getLinks();

			for(int i=0; i<xLinks.size();i++) { // for each y in neighbor_nodes(x)
				WayPoint y = xLinks.get(i);
				if(closedset.contains(y) || !y.getNavigable()) continue;  // if y in closedset already, go to next one

				float tentative_g_score = x.getG_Score() + 1; // NEW2
				boolean tentative_is_better = false;

				if (!openset.contains(y)) { // if y not in openset
					openset.add(y); // add y to openset
					tentative_is_better = true;
				} else if(tentative_g_score < y.getG_Score()) { // if tentative_g_score < g_score[y]
					tentative_is_better = true;
				} else
					tentative_is_better = false;

				if (tentative_is_better) {
					y.setPredecessor(x); // came_from[y] := x
				}

				y.setG_Score(tentative_g_score);
				y.setH_Score(y.getDistance(goal));
			} // while yIter.hasNext()
		} // while main loop
		Map.regenerate();// returns null if fails to find a  continuous path.
		return findPath(start, goal);
	}

	/**
	 * Finds the node within a set of neighbors with the least cost (potentially shortest distance to goal). 
	 * @return The node with the least cost.
	 */
	private static WayPoint getLowestCost(LinkedList<WayPoint> nodeSet) {
		WayPoint best = nodeSet.remove(nodeSet.size()-1);
		while(nodeSet.size()!=0) {
			WayPoint cur = nodeSet.remove(nodeSet.size()-1);
			if(cur.getF_Score() < best.getF_Score())
				best = cur;
		}
		return best;
	}
	
	/**
	 * Given the current node and the start node, this method retraces the completed path. It relies
	 * on WayPoint.getPredecessor() to backtrack using recursion. 
	 * 
	 * @param currentPoint
	 * @param start
	 * @param path The path output by this algorithm.
	 */
	private static final void reconstructPath(WayPoint currentPoint, WayPoint start, LinkedList <WayPoint> path){
		if(currentPoint != start)
			reconstructPath(currentPoint.getPredecessor(), start, path);
		path.add(currentPoint);
		return;
	}
	
}