package BrickOne.SecondaryController;
import Settings.Protocol;
import lejos.nxt.Battery;
import lejos.nxt.LCD;
import lejos.nxt.Sound;
import BrickOne.PrimaryController.RobotController;

/**
 * Handles all displayed input on brick
 * @author Bassam Riman
 *
 */
public class Display {
	//Up class
	//private RobotController robotController;
	//Timer
	//private Timer displayTimer;
	//private int period = SystemConstants.DisplayPERIOD;
	//private int displayProcessCount;
	//public static final int LCD_REFRESH = 500;
	//private Timer lcdTimer;
	private float[] pos = new float [3];
	
	// Data required from protocol
	private final int odometerInputProcessP = Protocol.odometerInputProcess;
	private final int xP = Protocol.X;
	private final int yP = Protocol.Y;
	private final int thetaP = Protocol.Theta;
	
	public Display(RobotController robotController){
		
		//this.displayTimer= new Timer(period, this);
		//this.robotController=robotController;
		//this.robotController.addTimer(this.displayTimer, this);
		LCD.clear();
		LCD.drawString("X:              ", 0, 0);
		LCD.drawString("Y:              ", 0, 1);
		LCD.drawString("T:              ", 0, 2);
		LCD.drawString("mV:", 0, 3);
		
	}

	public void updateDisplay(float[][] dataProcessed) {
		
		//Display Template
		LCD.clear();
		LCD.drawString("X:              ", 0, 0);
		LCD.drawString("Y:              ", 0, 1);
		LCD.drawString("T:              ", 0, 2);
		LCD.drawString("mV:", 0, 3);
		
		getRequiredDataFromDataColleted(dataProcessed);
		
		// print location
		for (int i = 0; i < 3; i++) {
			LCD.drawString(formattedDoubleToString(pos[i], 2), 3, i);
		}
		
		// also display battery voltage to test NiMH batteries
		int volts = Battery.getVoltageMilliVolt();
		if (volts>5400) LCD.drawInt(volts, 4, 3);
		else {
			Sound.playTone(440, 10000);
			LCD.clear();
			LCD.drawString("REPLACE BATTERIES!", 0, 0);
		}
		
	}
	public void getRequiredDataFromDataColleted(float[][] dataProcessed){
		pos[0]=dataProcessed[odometerInputProcessP][xP];
		pos[1]=dataProcessed[odometerInputProcessP][yP];
		pos[2]=dataProcessed[odometerInputProcessP][thetaP];
	}
	
	// method to convert a float to string, so we can print it
	private static String formattedDoubleToString(float x, int places) {
		String result = "";
		String stack = "";
		long t;
		
		// put in a minus sign as needed
		if (x < 0.0) result += "-";
		
		// put in a leading 0
		if (-1.0 < x && x < 1.0) result += "0";
		else {
			t = (long)x;
			if (t < 0) t = -t;
			while (t > 0) {
				stack = Long.toString(t % 10) + stack;
				t /= 10;
			}
			result += stack;
		}
		
		// put the decimal, if needed
		if (places > 0) {
			result += ".";
		
			// put the appropriate number of decimals
			for (int i = 0; i < places; i++) {
				x = Math.abs(x);
				x = (float) (x - Math.floor(x));
				x *= 10.0;
				result += Long.toString((long)x);
			}
		}
		
		return result;
	}
	
}