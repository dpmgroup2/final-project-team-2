package BrickOne.SecondaryController;

import java.util.LinkedList;

import Settings.SystemConstants;


/**
 * An object representing a point to navigate to. Has lots of calculation methods defined on it.
 * @author Jacob Barnett
 */

public class WayPoint {
	
	/**
	 * x coordinate, in feet
	 */
	private float x;
	
	/**
	 * y coordinate, in feet
	 */
	private float y;
	
	/**
	 * whether or not the point is navigable to
	 */
	private boolean navigable=true;
	
	/**
	 * the last point the A* came from
	 */
	private WayPoint cameFrom = null;
	
	/**
	 * A list representing a list of links between the way points
	 */
	private LinkedList <WayPoint> links = new LinkedList<WayPoint>();
	
	/**
	 * A variable used for the A* algorithm
	 */
	private float g_score = 0;	
	
	/**
	 * A variable used for the A* algorithm
	 */
	private float h_score = 0;
	
	/**
	 * Adds a link between two way points
	 * @param link the second point to be linked to
	 */
	public void addLink(WayPoint link){
		links.add(link);
	}
	
	/**
	 * @return the list of links between the way points
	 */
	public LinkedList<WayPoint> getLinks(){
		return links;
	}
	
	/**
	 * @param isNavigable whether or not a way point is navigable to
	 */
	public void setNavigable(boolean isNavigable){
		navigable=isNavigable;
	}
	
	/**
	 * @return whether or not the point is navigable to
	 */
	public boolean getNavigable(){
		return navigable;
	}
	
	/**
	 * @return the previous point on the path
	 */
	protected WayPoint getPredecessor() {
		return cameFrom;
	}
	
	/**
	 * @param orig the previous point in the path
	 */
	protected void setPredecessor(WayPoint orig) {
		cameFrom = orig;
	}
	
	/**
	 * @param newH score to be set for the A* algorithm
	 */
	protected void setH_Score(float newH) {
		h_score = newH;
	}
	
	/**
	 * @param newG score to be set for the A* algorithm
	 */
	protected void setG_Score(float newG) {
		g_score = newG;
	}
	
	/**
	 * @return G score from the A* algorithm
	 */
	protected float getG_Score(){
		return g_score;
	}
	
	/**
	 * @return F score from the A* algorithm
	 */
	protected float getF_Score(){
		return g_score + h_score;
	}
	
	
	/**
	 * Class constructor
	 * 
	 * @param x x coordinate in feet
	 * @param y y coordinate in feet
	 */
	public WayPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @param point2 second point to turn toward
	 * @return the correct heading TO turn to a second point
	 * @deprecated but left for potential utility
	 */
	public float getHeading(WayPoint point2) {
		
		//return 90 -  (Math.atan2(point2.getY() - this.y, point2.getX() - this.x)) / Constants.DEGRAD_CONVERT;
		
		float delY = point2.getY() - this.y, delX = point2.getX() - this.x, angle;
		
		if (delY == 0)	{	//when delta y = 0, an error occurs in calculating the atan
			angle =  (float) (delX / Math.abs(delX) * 90.0); //delta x / |delta x| gives 1 when delta x is positive and -1 when delta x is negative 
		} else if (delX == 0 && delY <0)	{ //will return 0 regardless of delta y, only needs correcting when delta y is negative
			angle =  180;
		} else {
			angle =  (float) (180.0 / Math.PI * Math.atan(delX/delY)); //arctangent of (delta x/delta y)
		}

		
		return angle;
	}
	
	/**
	 * Gets the angle to turn BY toward another point, given the robot's current orientation
	 * 
	 * @param point2 the point to turn to face
	 * @param curTheta the current orientation of the robot
	 * @return angle to turn by
	 */
	public float getAngle(WayPoint point2, float curTheta) {
		
		float delY = point2.getY() - this.y, delX = point2.getX() - this.x, angle;
		
		if (delY == 0)	{	//when delta y = 0, an error occurs in calculating the atan
			angle =  (float) (delX / Math.abs(delX) * 90.0); //delta x / |delta x| gives 1 when delta x is positive and -1 when delta x is negative 
		} else if (delX == 0 && delY <0)	{ //will return 0 regardless of delta y, only needs correcting when delta y is negative
			angle =  180;
		} else {
			angle =  (float) (180.0 / Math.PI * Math.atan(delX/delY)); //arctangent of (delta x/delta y)
		}
		
		if(delX > 0 && delY < 0) {
			angle = angle + 180;
		}

		if(delX < 0 && delY < 0) {
			angle = angle - 180;
		}
		
		return getAngularError(angle, curTheta);
	}
	
	/**
	 * 
	 * @param destTheta desired orientation
	 * @param curTheta current orientation
	 * @return the minimum angular difference between two angles
	 */
	public static float getAngularError(float destTheta, float curTheta) {
		//float destTheta = getHeading(point2);
		
		float error = destTheta - curTheta;
	
		
		
		if (error >= -180 && error <= 180) {
			return error;
		}
		else if (error < -180) {
			return error + 360;
		}
		else {
			return error - 360;
		}
	}
	
	/**
	 * @param point2 second point
	 * @param curTheta current orientation of the robot
	 * @return the forward error between the current position, and a second point
	 * @deprecated but left for potential utility
	 */
	public float getForwardError(WayPoint point2, float curTheta) {
		
		//return (Math.cos(curTheta) * (point2.getX() - this.x)) + (Math.sin(curTheta) * (point2.getY() - this.y));
		
		float v11, v12, v21, v22;
		
		v11 = this.x;
		v12 = this.y;
		v21 = point2.getX();
		v22 = point2.getY();
		
		v21 -= v11;
		v22 -= v12;
		
		float vMag = (float) Math.sqrt((v11 * v11) + (v12 * v12));
		if (vMag != 0) {
			v11 /= vMag;
			v12 /= vMag;
		}
		
		return (v11 * v21) + (v12 * v22);
	}
	
	/**
	 * @param point2 second point
	 * @return the distance in feet between two points
	 */
	public float getDistance(WayPoint point2) {
		return (float)Math.sqrt( Math.pow((point2.getX() - this.x), 2) +  Math.pow((point2.getY() - this.y), 2));
	}

	/**
	 * @return a String representing a WayPoint
	 */
	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}
	
	/**
	 * Checks if two points are equal, as defined as being close within a centimeter
	 * 
	 * @param p second point
	 * @return whether the points can be considered equal
	 */
	public boolean isEqual(WayPoint p) {
		return this.getDistance(p)<(1/SystemConstants.foot);
	}
	
	// getters and setters
	
	/**
	 * @return x value of the point, in foot
	 */
	public float getX() {
		return this.x;
	}
	
	/**
	 * @param newX new x value to be set
	 */
	public void setX(float newX) {
		this.x = newX;
	}

	/**
	 * @return y value of the point, in feet
	 */
	public float getY() {
		return this.y;
	}

	/**
	 * @param newY new y value to be set, in feet
	 */
	public void setY(float newY) {
		this.y = newY;
	}
	
	/**
	 * @return x cm value of the point
	 */
	public float getXcm() {
		return x*SystemConstants.foot;
	}

	/**
	 * @param newX new x grid value to be set
	 */
	public void setXcm(float newX) {
		this.x = newX/SystemConstants.foot;
	}

	/**
	 * @return y cm value of the point
	 */
	public float getYcm() {
		return y*SystemConstants.foot;
	}

	/**
	 * 
	 * @param newY new y grid value to be set
	 */
	public void setYcm(float newY) {
		this.y = newY/SystemConstants.foot;
	}
}