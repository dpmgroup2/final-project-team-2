package BrickOne.SecondaryController;

import java.util.ArrayList;

import BrickOne.PrimaryController.RobotController;
import Settings.MapProtocol;
import Settings.SystemConstants;

public class Map {
	/**
	 * list of map
	 */
	public static ArrayList<WayPoint> mapArray;
	
	/**
	 * Constructor
	 */
	public Map(){

		mapArray = new ArrayList<WayPoint>(121);
		generateMap();
		setForbidden();
		
	}
	
	public static ArrayList<WayPoint> getMap() {
		return mapArray;
	}

	/**
	 * generates a list of map
	 */
	public static void generateMap() {
		for(int y=0; y<11; y++)
			for(int x=0; x<11; x++) {
				mapArray.add(new WayPoint (x, y));
			}
		
		for(int i=0; i<121; i++)
			for(int j=0; j<MapProtocol.a[i].length; j++)
				mapArray.get(i).addLink(mapArray.get(MapProtocol.a[i][j]));
	}
	

	public static void regenerate() {
		for(int i=0; i<121; i++) mapArray.get(i).setNavigable(true);
		setForbidden();
		
	}
	
	public static void setForbidden() {
		for(int i=5-(int)(RobotController.btData[3]/2); i<5+(int)(RobotController.btData[3]/2); i++)
			for(int j=10; j>=(10-RobotController.btData[4]); j--)
				mapArray.get(i+j*11).setNavigable(false);
	}
	
	public WayPoint getWayPoint(int xFoot, int yFoot){
		if (xFoot>10) xFoot=10;
		else if (xFoot<0) xFoot=0;
		if (yFoot>10) yFoot=10;
		else if (yFoot<0) yFoot=0;
		return mapArray.get(yFoot*11+xFoot);
	}
	
	public WayPoint getWayPoint(WayPoint point){
		int x = (int)Math.round(point.getX());
		int y = (int)Math.round(point.getY());
		return getWayPoint(x,y);
	}
	
	public WayPoint getWayPointFromCentimeter(float x, float y){
		return getWayPoint( getfoot(x),getfoot(y));
	}
	
	public int getfoot(double x) {
		int temp=(int)Math.round(x/SystemConstants.foot);
		if (temp>10) temp=10;
		else if (temp<0) temp=0;
		return temp;
	}
}
