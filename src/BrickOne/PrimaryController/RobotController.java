package BrickOne.PrimaryController;


import lejos.util.Delay;
import lejos.util.Timer;
import BrickCommunication.B1toB2Connection;
import BrickOne.Input.Input;
import BrickOne.Input.UltrasonicSensorController;
import BrickOne.InputProcessor.InputProcessor;
import BrickOne.InputProcessor.OdometerInputProcess;
import BrickOne.SecondaryController.Display;
import BrickOne.SecondaryController.Map;
import BrickOne.SecondaryController.WayPoint;
import Settings.Protocol;
import Settings.RobotSettings;

/**
 * Controls the robot, handles permission request from output processors, handles triggers and info from input processors and contains all the robot info
 * @author Bassam Riman
 *
 */
public class RobotController {
	private InputProcessor inputProcessor;
	private Display display;
	private B1toB2Connection comm;
	private Driver go;
	public static int robotStep=-1;
	private byte shootRange=9;
	private byte shootSuccess=0;
	private byte shootSpot=0;
	private byte shootIncrement=1;
	private byte[] shootBan={1,1,1,1,1};
	private float[][][] shootSpots={
		{{1,	(float)4.2554,	4,(float)209.8499/*204,214.8499*/},
		{3,		(float)3.2918,	3,(float)190.6015/*185,196.6015*/},
		{5, 	(float)3,	 	3,(float)185},//180
		{7, 	(float)3.2918,	3,(float)166.3985},//158
		{9,		(float)4.2554, 	4,(float)149.6501/*145.1501*/}},
		
		{{1, 	(float)3.0718,	3,(float)209},//210
		{3, 	(float)2.2540,	2,(float)197.4775},//194.4775
		{5, 	(float)2,	 	2,(float)184},//180
		{7, 	(float)2.2540,	2,(float)168.5225},//165.5225
		{9,		(float)3.0718,	3,(float)153}},//150
		
		{{1,	(float)1.9377,	2,(float)210.3878},//206.3878
		{3, 	(float)1.225,	1,(float)187.8396},//192.8396
		{5, 	(float)1,	 	1,(float)185},//180
		{7, 	(float)1.225,	1,(float)173.1604},//167.1607
		{9,		(float)1.9377,	2,(float)156.6122}}};//153.6122

	
	public static WayPoint dispenserLoc=new WayPoint(0,0);
	public static WayPoint defenceLoc=new WayPoint(0,0);
	public static boolean gotBallsOfSteel=true;
	
	
	
	public static int[] btData={1,11,4,2,2,5,2};
	public static boolean btDone=false;
	private int dispenser=-1;
	//all timer list, add new timer here and in method addTimer
	private Timer inputTimer;

	
	
	
	public RobotController(){
		comm=new B1toB2Connection();
		inputProcessor=new InputProcessor(this, comm);
		go=new Driver((new Localization((OdometerInputProcess)inputProcessor.
				getInputProcessHolder()[Protocol.odometerInputProcess])));
		
		if(RobotSettings.displayON){
			display=new Display(this);
		}
		
		inputTimer.start();

	}
	/**
	 * Run method of the RobotController thread
	 */
	public void run(float[][] dataProcessed) {
		if(RobotSettings.displayON){
			display.updateDisplay(dataProcessed);
		}
		int check;
		//LOGICS
		
		switch(robotStep) {
			case -1: comm.connect();
					btData=comm.getThis(7);
					btDone=true;
					robotStep=0;
					break;
			case 0:	if(go.update(dataProcessed)==100) robotStep=1;
					break;
			case 1: Driver.turnTo(0, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
					robotStep=2;
					break;
			case 2: if(!Driver.isNavigating()) robotStep=4;
					break;
//			case 3:	comm.connect();
//					btData=comm.getThis(7);
//					btDone=true;
//					robotStep=4;
//					break;
			case 4: Map.setForbidden();
					if(btData[6]!=1) go.updateCurPoint();
					//compute range
					if(btData[5]==5) shootRange=7;
					else if(btData[5]==6) shootRange=8;
					
					
					//travel to defend or to get balls
					if(btData[0]==2) {
						robotStep=89;

						if(btData[6]==1 || btData[6]==4) go.setDestination(2, 9);
						else go.setDestination(8, 9);

						if(btData[6]==1 || btData[6]==4) go.setDestination(2, 10);
						else go.setDestination(8, 10);
						Driver.mandatory=true;

					}
					else {
						if(btData[1]==-1) {
							btData[1]=0;
							dispenser=90;
						}
						else if(btData[1]==11) {
							btData[1]=10;
							dispenser=270;
						}
						
						if(btData[2]==-1) {
							btData[2]=0;
							dispenser=0;
						}
						else if(btData[2]==11) {
							btData[2]=10;
							dispenser=180;
						}
						if(btData[1]>5) {
							shootIncrement=-1;
							shootSpot=4;
						}

						dispenserLoc=Map.mapArray.get(btData[1]+11*btData[2]);
						gotBallsOfSteel=false;
						robotStep=30;
					}
					break;
					
					//go defend
			case 89:if(go.update(dataProcessed)==100){
					Driver.mandatory=false;
					if(btData[6]==1 || btData[6]==4) go.addWayPoint((float)2.9, (float)9.9);
					else go.addWayPoint((float)7.1, (float)9);
						Driver.firstEmpty=0;
						robotStep=90;
					}
					break;
			case 90:if(go.update(dataProcessed)==100) robotStep=91;
					break;	
			case 91:if(btData[6]==1 || btData[6]==4) Driver.turnTo(270, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
					else Driver.turnTo(80, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
					robotStep=92;
					break;
			case 92:if(!Driver.isNavigating()) robotStep=93;
					break;
			case 93:comm.deployDef();
					robotStep=65;
					break;
					
					
					
					//go get balls
			case 30:go.setDestination(dispenserLoc);
					robotStep=31;
					break;
			case 31:check=go.update(dataProcessed);
					if(check==100){
						gotBallsOfSteel=true;
						Driver.firstEmpty=0;
						go.wayPoints.add(dispenserLoc);
						robotStep=32;
					}
					else if(check==999)robotStep=30;
					break;
			case 32:if(go.update(dataProcessed)==100) robotStep=50;
					break;
							
					
					//back and get balls
			case 50:if(go.update(dataProcessed)==100) robotStep=51;
					break;
			case 51:if(dispenser!=-1) Driver.turnTo(dispenser, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
					robotStep=52;
					break;
			case 52:if(!Driver.isNavigating()) robotStep=53;
					break;

			case 53:Driver.moveCM(-8);
					robotStep=54;
					break;
			case 54:if(!Driver.isNavigating()) robotStep=55;
					break;
			case 55:Delay.msDelay(RobotSettings.ballDelay);
					robotStep=56;
					break;
					
					
					//travel to shooting spots
			case 56:if(shootSuccess==RobotSettings.howManyShots) {
						gotBallsOfSteel=false;
						robotStep=30;
						shootSuccess=0;
						break;
					}
					else {
						byte temp=0;
						while(shootBan[shootSpot]==0) {
							shootSpot+=shootIncrement;
							temp++;
							if(shootSpot==4) shootSpot=0;
							else if(shootSpot==0) shootSpot=4;
							if(temp==RobotSettings.howManyShots) 
								for(int q=0; q<RobotSettings.howManyShots; q++)
									shootBan[q]=1;
						}
					}
					robotStep=57;
					break;
			case 57:go.setDestination((int)shootSpots[shootRange-7][shootSpot][Protocol.shootXft],
										(int)shootSpots[shootRange-7][shootSpot][Protocol.shootYclose]);
					robotStep=58;
					go.update(dataProcessed);
					break;
			case 58:check=go.update(dataProcessed);
					if(check==100) robotStep=59;
					else if(check==999) {
						shootBan[shootSpot]=0;
						robotStep=56;
					}
					break;
			case 59:if(shootSpot==2) {
						robotStep=61;
						break;
					}
					Driver.firstEmpty=0;
					go.addWayPoint(shootSpots[shootRange-7][shootSpot][Protocol.shootXft],
									shootSpots[shootRange-7][shootSpot][Protocol.shootYft]);
					robotStep=60;
					break;
			case 60:check=go.update(dataProcessed);
					if(check==100) robotStep=61;
					else if(check==999) {
						shootBan[shootSpot]=0;
						robotStep=56;
					}
					break;
			case 61:Driver.turnTo(shootSpots[shootRange-7][shootSpot][Protocol.shootTheta], dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
					robotStep=62;
					break;
			case 62:if(!Driver.isNavigating()) robotStep=63;
					break;
			case 63:UltrasonicSensorController.disableUS();
					for(int qwerty=0; qwerty<RobotSettings.howManyShots; qwerty++){
						comm.fire(shootRange);
						while(!comm.fireDone()) Delay.msDelay(500);
						Delay.msDelay(10000);
					}
					robotStep=65;
					break;
			case 64:if(comm.fireDone()) {
						robotStep=56;
						shootSpot+=shootIncrement;
						if(shootSpot==-1) shootSpot=4;
						else if(shootSpot==5) shootSpot=0;
						shootSuccess++;
						UltrasonicSensorController.enableUS();
					}
					break;
			case 65:inputTimer.stop();
					comm.close();
					System.exit(0);
					break;
			default: break;
		}
	}
	
	
	public void addTimer(Timer timer, Object object) {
		// Add an if statement here if you added a new timer
		if (object instanceof Input){
			this.inputTimer=timer;
		}
	}
}