package BrickOne.PrimaryController;

import Settings.Protocol;
import Settings.SystemConstants;
import lejos.nxt.Sound;
import BrickOne.InputProcessor.OdometerInputProcess;


/**
 * process the output part of localization, controls wheels of the robot if permission grant by RobotController
 * @author Bassam Riman
 *
 */
public class Localization {
	public OdometerInputProcess odo;
	private float angleA, angleB, tempTheta;
	private float[] angles = {0, 0, 0, 0};
	private int i;
	private int locStep=0;
	private int rightTicks=0;
	private boolean onTheFly=false;
	
	public Localization(OdometerInputProcess odo) {
		this.odo=odo;
	}

  	public boolean localize(float[][] dataProcessed){
  		int dist = (int)dataProcessed[Protocol.localizationInputProcess][Protocol.fUSout];
  		
  		switch(locStep) {
	  		case 99:return true;
	  		// start a long rotation
	  		case 0:	Driver.clockWise(720);
					locStep=1;
					break;
					
			// poll every 50 ms until it doesn't see a wall anymore +-1 cm error bandgap everywhere
	  		case 1:	if(dist-1>SystemConstants.usLocThresh) locStep=2;
	  				break;
	  				
	  		// poll until it sees a wall again and latch the angle
	  		case 2:	if(dist+1<SystemConstants.usLocThresh) {
	  	  				angleA = dataProcessed[Protocol.odometerInputProcess][Protocol.Theta];
	  	  				Driver.stop();
	  	  				locStep=3;
	  	  				Sound.beep();
	  	  			}
	  				else break;
				  	// switch direction and wait until it sees no wall
				  	Driver.counterClockWise(720);
				  	break;
	  		case 3:	if(dist-1>SystemConstants.usLocThresh) locStep=4;
	  				break;
	  				// keep rotating until the robot sees a wall, then latch the angle
	  		case 4:	if(dist+1<SystemConstants.usLocThresh) {
						angleB = dataProcessed[Protocol.odometerInputProcess][Protocol.Theta];
						Driver.stop();
						locStep=5;
						Sound.beep();
					}
	  				else break;
					// compute the values
					if(angleA<angleB) tempTheta = 225+(angleB-angleA)/2;
					else tempTheta = 45+(angleB-angleA)/2;
					// update the odometer's angle and travel to a location that allows light localization
					if (tempTheta>360) tempTheta=tempTheta%360;
					else if (tempTheta<0) tempTheta=360+tempTheta%360;
					break;
	  		case 5:	if(!Driver.isNavigating()) {
	  					odo.setTheta(tempTheta);
	  					Driver.turnTo(115, tempTheta);
						locStep=6;
	  				}
	  				break;
	  				
	  		case 6:	if(!Driver.isNavigating()) locStep=7;
	  				else break;
	  		
	  		// start a 360 turn
	  		case 7:	Driver.counterClockWise(360);
	  				i=0;
					locStep=8;
					break;
	  		case 8:	if(Driver.isNavigating()) {
	  					// detect all 4 lines
	  					if(i<4){
	  						// polling loop until a drop of over 50 units from the previous is detected
	  						if((int)dataProcessed[Protocol.localizationInputProcess][Protocol.rLSout]==1) {
	  							Sound.beep();
	  							// save the angle
	  							angles[i]=(float) (dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]-87.102);
	  							if(angles[i]<0) angles[i]+=360;
	  							i++;
	  						}
	  					}
	  					else {
	  						// as soon as the 4th line is detected, stop rotating to save time
	  						Driver.stop();
	  						float[] pos = new float[3];
	  						float thYm, thXp, thYp, thXm, newX, newY;
	  						// do the math according to the tutorial
	  						thYm=angles[0];
	  						thXp=angles[1];
	  						thYp=angles[2];
	  						thXm=angles[3];
	  						newX=(float) (-SystemConstants.lsOffset*Math.cos(Math.toRadians((thYm-thYp)/2)));
	  						newY=(float) (-SystemConstants.lsOffset*Math.cos(Math.toRadians((thXp-thXm)/2)));
	  						pos[2]=dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]+SystemConstants.angleOffset-(thYm+thYp+thXm+thXp)/4;
	  							  						
	  						if(pos[2]>360) pos[2]-=360;
	  						if(pos[2]<0) pos[2]+=360;
						
							pos[0]=Driver.nextPoint.getXcm()+newX;
							pos[1]=Driver.nextPoint.getYcm()+newY;
							odo.setPosition(pos);
	 
	  						locStep=99;
	  					}
	  				}
	  				else locStep=7;
	  				break;
	  				
	  		case 9:	Driver.counterClockWise(90);
	  				locStep=10;
	  				rightTicks=0;
	  				break;
	  				
	  		case 10:if(Driver.isNavigating()) rightTicks+=(int)dataProcessed[Protocol.localizationInputProcess][Protocol.rLSout];
	  				else {
						if(rightTicks>0) locStep=11;
						else locStep=13;
	  				}
					break;
					
	  		case 11:Driver.moveCM(-10);
	  				locStep=12;
	  				break;
	  				
	  		case 12:if(!Driver.isNavigating()) locStep=13;
					else break;
	  		
	  		case 13:Driver.moveCM(10);
	  				locStep=14;
	  				break;
	  				
	  		case 14:if(Driver.flyAlign(dataProcessed, false)) {
	  					if(!onTheFly) {
	  						locStep=9;
	  						onTheFly=true;
	  					}
	  					else locStep=99;
	  				}
	  				break;
	  		
	  		case 15:if(dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]>330 || dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]<30) {
	  					locStep=10;
	  					rightTicks=0;
	  				}
	  				break;

	  		case 16:Driver.turnTo(225, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
	  				locStep=17;
	  				break;
	  				
	  		case 17:if(!Driver.isNavigating()) locStep=18;
	  				else break;
	  		
	  		case 18:Driver.moveCM(5);
	  				locStep=19;
	  				break;
	  				
	  		case 19:if(!Driver.isNavigating()) locStep=20;
					else break;
	  		
	  		case 20:Driver.turnTo(115, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
	  				locStep=6;
	  				break;
	  				
	  		default:break;
  		}
  		return false;
  	}
  	
  	public void startInitialLocalization(float[][] dataProcessed) {
  		locStep=0;
  		localize(dataProcessed);
  	}
  	
  	public void startFastLocalization(float[][] dataProcessed, boolean onTheFly) {
  		this.onTheFly=onTheFly;
  		if(onTheFly) locStep=9;
  		else {
  			Driver.turnTo(0, dataProcessed[Protocol.odometerInputProcess][Protocol.Theta]);
  			locStep=15;
  		}
  		localize(dataProcessed);
  	}
  	
  	public void startAwesomeLocalization(float[][] dataProcessed) {
  		locStep=16;
  		localize(dataProcessed);
  	}
  	
}