package BrickOne.PrimaryController;

/**
 * Navigation thread to run in the background.
 * Allows the robot to navigate along a path from point to point,
 * while avoiding obstacles.
 * 
 * If an obstacle is seen in front, a path around the obstacle is drawn.
 * 
 * If an obstacle is seen at the side, the robot will start a p-type
 * wall following routine to go around it.
 * 
 * @author Jacob Barnett
 */

import java.util.LinkedList;

import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import BrickOne.SecondaryController.Astar;
import BrickOne.SecondaryController.Map;
import BrickOne.SecondaryController.WayPoint;
import Settings.Protocol;
import Settings.RobotSettings;
import Settings.SystemConstants;

public class Driver{
	private static final int SPEED = 200;

	public LinkedList<WayPoint> wayPoints = new LinkedList<WayPoint>();
	public static WayPoint nextPoint, prevPoint;
	public Map map;
	public static int firstEmpty=0;
	public static boolean rightStop = true, leftStop=true, skipPrevPoint=false;
	private static int driverStep=-3;
	private int wayNo=-1;
	private boolean onTheFly=true;
	
	/*
	 * -1 is fast localization
	 * -2 wait for localization to complete
	 * -3 is initial localization
	 * -4 once localization is finished, travel to last waypoint (by default at 0,0)
	 * -5 start awesome localization at the end of waypoints
	 * 0 is start including turnTo
	 * 1 wait for turn to complete
	 * 2 go forward
	 * 3 activate light when close
	 * 4 stop on the line and update odo
	 * 99 is travelTo request new waypoint
	 * 100 is no more waypoints
	 * 101 is request fast localization
	 * 911 emergency
	 */
	
	public static NXTRegulatedMotor leftMotor = Motor.A;
	public static NXTRegulatedMotor rightMotor = Motor.B;
	public static Localization loc;
	
	
	public Driver(Localization loc) {
		// reset the motors
		for (NXTRegulatedMotor motor : new NXTRegulatedMotor[] { leftMotor, rightMotor }) {
			motor.stop();
			motor.setAcceleration(SystemConstants.acceleration);
		}
		
		Driver.loc = loc;

		map=new Map();
		nextPoint=map.getWayPoint(0, 0);
		prevPoint=nextPoint;
	}
	public static boolean mandatory=false;

	/**
	 * Updates the driver with new information, and processes what should be done this current step
	 * 
	 * @param dataProcessed a 2D array with all of the robot's current information
	 */
	public int update(float[][] dataProcessed) {
		
		switch(driverStep) {
			case 100:if(!wayPoints.isEmpty()) {
						driverStep=0;
						break;
					}
					return 100;
			case 910:if(!skipPrevPoint)moveCM(-10);
					driverStep=911;
					break;
			case 911:if(!isNavigating()) driverStep=912;
					break;
			case 999:
					if(!wayPoints.isEmpty()) {
						driverStep=0;
						break;
					}
					return 999;
			case 912:WayPoint destination=new WayPoint(0,0);
					if(wayPoints.isEmpty())	{
						if(RobotController.gotBallsOfSteel){
							driverStep=999;
							break;
						}
					}
					else destination = wayPoints.remove(wayPoints.size() - 1);
					
					if(!skipPrevPoint) {
						wayPoints.clear();
						nextPoint.setNavigable(false);
						if(!RobotController.gotBallsOfSteel) {
							wayPoints=Astar.findPath(prevPoint, RobotController.dispenserLoc);
							wayPoints.remove(wayPoints.size()-1);
						}
						else wayPoints=Astar.findPath(prevPoint, destination);
							
						driverStep=0;
						skipPrevPoint=true;
						break;
					}
					else {
						skipPrevPoint=false;
						LinkedList<WayPoint> links = nextPoint.getLinks();
						int w=0;
						while(!links.get(w).getNavigable() && w<links.size()) w++;
						if(w>links.size()) w=links.size()-1;
						wayPoints.clear();
						if(!RobotController.gotBallsOfSteel) {
							wayPoints=Astar.findPath(links.get(w), RobotController.dispenserLoc);
							wayPoints.remove(wayPoints.size()-1);
						}
						else wayPoints=Astar.findPath(links.get(w), destination);
						driverStep=0;
						break;
					}
			case 99:skipPrevPoint=false;
					if(!wayPoints.isEmpty()) {
						driverStep=0;
						wayNo++;
						break;
					}
					else {
						if(firstEmpty==0) {
							driverStep=100;
							firstEmpty=2;
							prevPoint=nextPoint;
							break;
						}
						else if(firstEmpty==1){
							wayNo=0;
							driverStep=-5;
							firstEmpty=0;
							break;
						}
						else{
							wayNo=0;
							driverStep=-1;
							firstEmpty=1;
							break;
						}
					}
			case 0: if(wayNo==RobotSettings.stepsBeforeLoc) {
						wayNo=0;
						driverStep=-1;
						break;
					}
					prevPoint=nextPoint;
					nextPoint=wayPoints.remove(0);
					if(mandatory && wayPoints.isEmpty()) firstEmpty=0;
					travelTo(dataProcessed);
					break;
			case 1:	case 2: case 6: case 7:
					travelTo(dataProcessed);
					break;
			case -1:loc.startFastLocalization(dataProcessed, onTheFly);
					driverStep=-2;
					break;
			case -2:if(loc.localize(dataProcessed)) driverStep=-4;
					break;
			case -3:loc.startInitialLocalization(dataProcessed);
					driverStep=-2;
					break;
			case -4:driverStep=0;
					travelTo(dataProcessed);
					break;
			case -5:loc.startAwesomeLocalization(dataProcessed);
					driverStep=-2;
			default:if(!(firstEmpty==0 && wayPoints.isEmpty()))checkEmergency(dataProcessed);
					travelTo(dataProcessed);
					break;
		}
		
		return 99;
	}

	public void updateCurPoint() {
			if(RobotController.btData[6]==2) {
				prevPoint=Map.mapArray.get(10);
			}
			else if(RobotController.btData[6]==3) {
				prevPoint=Map.mapArray.get(120);
			}
			else if(RobotController.btData[6]==4) {
				prevPoint=Map.mapArray.get(110);
			}
			
			loc.odo.setX(prevPoint.getXcm());
			loc.odo.setY(prevPoint.getYcm());
			loc.odo.setTheta(450-90*RobotController.btData[6]);
			
	}
	
	/**
	 * Adds a new gridPoint to the robot's path
	 * 
	 * @param x
	 * @param y
	 */
	public void addWayPoint(float x, float y) {
		wayPoints.add(new WayPoint(x, y));
	}
	
	/**
	 * Constructs a path to follow along the grid.
	 * The path it follows is such as to allow for line correction.
	 * 
	 * @param x destination point
	 * @param y destination point
	 * @param dataProcessed 2D array containing the robot's current data
	 */
	public void setDestination(int x, int y) {
		wayPoints.clear();
		wayPoints=Astar.findPath(map.getWayPoint(prevPoint), map.getWayPoint(x, y));

		
	}
	
	public void setDestination(WayPoint point){
		if(!RobotController.gotBallsOfSteel) {
			wayPoints.clear();
			wayPoints=Astar.findPath(prevPoint, point);
			wayPoints.remove(wayPoints.size()-1);
		}
		else setDestination((int)Math.round(point.getX()), (int)Math.round(point.getY()));
	}
	
	/**
	 * Travel to the next point, as previously determined by the method {@link #update}
	 * 
	 * @param point next point to travel to
	 */
	public void travelTo(float[][] dataProcessed) {		

		WayPoint curPoint = new WayPoint(dataProcessed[Protocol.odometerInputProcess][Protocol.X]/SystemConstants.foot,
				dataProcessed[Protocol.odometerInputProcess][Protocol.Y]/SystemConstants.foot);
		float tempTheta = dataProcessed[Protocol.odometerInputProcess][Protocol.Theta];
		int tempL, tempR;
		switch(driverStep) {
			default:break;
			case 0:	if(curPoint.isEqual(nextPoint)) driverStep=7;
					else {
						turnBy(curPoint.getAngle(nextPoint, tempTheta));
						driverStep=1;
					}
					break;
			case 1:	if(!isNavigating()) driverStep=2;
					break;
			case 2: if(curPoint.isEqual(map.getWayPoint(curPoint)) && Map.mapArray.contains(nextPoint)) {
						if(tempTheta < 5 || tempTheta > 355) driverStep=3;
						else if(tempTheta > 85 && tempTheta < 95) driverStep=3;
						else if(tempTheta >175 && tempTheta < 185) driverStep=3;
						else if(tempTheta > 265 && tempTheta < 285) driverStep=3;
						else driverStep=6;
					}
					else driverStep=6;
					leftMotor.setSpeed((int) (SPEED*SystemConstants.rWheel/SystemConstants.lWheel));
					rightMotor.setSpeed(SPEED);
					break;
			case 3: tempL=convertDistance(SystemConstants.lWheel, curPoint.getDistance(nextPoint)*SystemConstants.foot+15);
					tempR=convertDistance(SystemConstants.rWheel, curPoint.getDistance(nextPoint)*SystemConstants.foot+15);
					leftMotor.rotate(tempL, true);
					rightMotor.rotate(tempR, true);
					driverStep=4;
					onTheFly=true;
					break;
			case 4: if(nextPoint.getDistance(curPoint)<0.5) {
						driverStep=5;
						leftMotor.setSpeed((int) (SPEED*SystemConstants.rWheel/SystemConstants.lWheel/1.5));
						rightMotor.setSpeed((int)(SPEED/1.5));
					}
					break;
			case 5:	flyAlign(dataProcessed, true);
					break;
			case 6: tempL=convertDistance(SystemConstants.lWheel, curPoint.getDistance(nextPoint)*SystemConstants.foot);
					tempR=convertDistance(SystemConstants.rWheel, curPoint.getDistance(nextPoint)*SystemConstants.foot);
					leftMotor.rotate(tempL, true);
					rightMotor.rotate(tempR, true);
					driverStep=7;
					onTheFly=false;
					break;
			case 7: if(!isNavigating()) {
						driverStep=99;
					}
			}
	}
			
	/**
	 * turn BY a given angle
	 * 
	 * @param theta angle to turn by
	 */
	public static void turnBy(float theta) {
		
		//TODO leftMotor.setSpeed((int)(SPEED*SystemConstants.rWheel/SystemConstants.lWheel));
		leftMotor.setSpeed(SPEED);
		rightMotor.setSpeed(SPEED);
		int tempL=convertAngle(SystemConstants.lWheel, SystemConstants.width, theta),
			tempR=-convertAngle(SystemConstants.rWheel, SystemConstants.width, theta);
		leftMotor.rotate(tempL, true);//TODO CARE!!!!
		rightMotor.rotate(tempR, true);
	}
	

	/**
	 * turn TO a given angle
	 * 
	 * @param theta angle to turn to
	 * @param angleNow current orientation
	 */
	public static void turnTo(float theta, float angleNow) {
		turnBy(WayPoint.getAngularError(theta, angleNow));
	}
	

	/**
	 * @param radius the radius of a wheel
	 * @param distance a distance on the field to convert for rotation
	 * @return a distance for the wheels to rotate
	 */
	private static int convertDistance(float radius, float distance) {
		return (int) ((180.0 * distance) / (Math.PI * radius));
	}

	/**
	 * @param radius radius of the wheel
	 * @param width width of the robot
	 * @param angle angle to convert
	 * @return an angle for the wheels to rotate
	 */
	private static int convertAngle(float radius, float width, float angle) {
		return convertDistance(radius, (float)(Math.PI * width * angle / 360.0));
	}
	
	/**
	 * a public method to stop the motors
	 */
	public static void stop(){
		leftMotor.stop(true);
		rightMotor.stop(true);
	}
	
	/**
	 * @return whether the robot is moving (navigating)
	 */
	public static boolean isNavigating (){
		return motorMoving(leftMotor) || motorMoving(rightMotor);
	}
	
	/**
	 * @param motor motor to test
	 * @return whether a motor is moving (rotating)
	 */
	public static boolean motorMoving(NXTRegulatedMotor motor) {
		return Math.abs(motor.getRotationSpeed()) > 1;
	}
	/**
	 * rotate the robot counter-clockwise by an angle
	 * @param angle angle to rotate by
	 */
	public static void counterClockWise (int angle){
		leftMotor.setSpeed((int)(SPEED*SystemConstants.rWheel/SystemConstants.lWheel)/2);
		//leftMotor.setSpeed(SPEED/2);
		rightMotor.setSpeed(SPEED/2);
		int tempL = -convertAngle(SystemConstants.lWheel, SystemConstants.width, angle);
		int tempR = convertAngle(SystemConstants.rWheel, SystemConstants.width, angle);
		leftMotor.rotate(tempL, true);
		rightMotor.rotate(tempR, true);
	}

	/**
	 * rotate the robot clockwise by an angle
	 * @param angle angle to rotate by
	 */
	public static void clockWise (int angle){
		leftMotor.setSpeed((int)(SPEED*SystemConstants.rWheel/SystemConstants.lWheel)/2);
		//leftMotor.setSpeed(SPEED/2);
		rightMotor.setSpeed(SPEED/2);
		int tempL = convertAngle(SystemConstants.lWheel, SystemConstants.width, angle);
		int tempR = -convertAngle(SystemConstants.rWheel, SystemConstants.width, angle);
		leftMotor.rotate(tempL, true);
		rightMotor.rotate(tempR, true);
	}
	
	/**
	 * moves the robot by a distance
	 * @param distance distance to travel
	 */
	public static void moveCM(float distance) {
		leftMotor.setSpeed((int) (SPEED*SystemConstants.rWheel/SystemConstants.lWheel));
		rightMotor.setSpeed(SPEED);
		int tempL = convertDistance(SystemConstants.lWheel, distance);
		int tempR = convertDistance(SystemConstants.rWheel, distance);
		leftMotor.rotate(tempL, true);
		rightMotor.rotate(tempR, true);
	}
	
	/**
	 * routine to execute on-the-fly alignment
	 * 
	 * @param dataProcessed 2D array of current data
	 * @param inside whether or not the method is called from inside Driver
	 * @return
	 */
	public static boolean flyAlign(float[][] dataProcessed, boolean inside) {

		float tempTheta = dataProcessed[Protocol.odometerInputProcess][Protocol.Theta];
		
		if(dataProcessed[Protocol.driverInputProcess][Protocol.rLS]==1 && rightStop) {
			rightMotor.setAcceleration(99999);
			rightMotor.stop(true);
			rightStop=false;
		}
		if(dataProcessed[Protocol.driverInputProcess][Protocol.lLS]==1 && leftStop) {
			leftMotor.setAcceleration(99999);
			leftMotor.stop(true);
			leftStop=false;
		}
		
		
		if(!motorMoving(rightMotor) && !motorMoving(leftMotor)) {
			leftMotor.setAcceleration(SystemConstants.acceleration);
			rightMotor.setAcceleration(SystemConstants.acceleration);
			if(inside) driverStep=99;
		}
		else return false;
		
		
		
		if(!leftStop && !rightStop) {	

			int angleNailing=SystemConstants.angleNailing;
			//y positive
			if(tempTheta < angleNailing || tempTheta > 360-angleNailing) {
				loc.odo.setTheta(0);
				loc.odo.setY(nextPoint.getYcm());
			}
			//x positive
			else if(tempTheta > 90-angleNailing && tempTheta < 90+angleNailing) {
				loc.odo.setTheta(90);
				loc.odo.setX(nextPoint.getXcm());
			}
			//y negative
			else if(tempTheta >180-angleNailing && tempTheta < 180+angleNailing) {
				loc.odo.setTheta(180);
				loc.odo.setY(nextPoint.getYcm());
			}
			//x negative
			else if(tempTheta > 270-angleNailing && tempTheta < 270+angleNailing) {
				loc.odo.setTheta(270);
				loc.odo.setX(nextPoint.getXcm());
			}
		}
		leftStop=true;
		rightStop=true;
		return true;
	}

	/**
	 * @param dataProcessed 2D array of current data
	 * @return a byte representing the kind of emergency, based on the data
	 */
	private void checkEmergency(float[][] dataProcessed) {
		//touch emergencies

		//US emergencies
		if (dataProcessed[Protocol.driverInputProcess][Protocol.fTS] == 1
				|| dataProcessed[Protocol.driverInputProcess][Protocol.fUS]  < SystemConstants.usFrontThresh
				|| dataProcessed[Protocol.driverInputProcess][Protocol.lUS] < SystemConstants.usSideThresh
				|| dataProcessed[Protocol.driverInputProcess][Protocol.rUS] < SystemConstants.usSideThresh) {
			leftMotor.setAcceleration(99999);
			rightMotor.setAcceleration(99999);
			leftMotor.stop(true);
			rightMotor.stop(false);
			leftMotor.setAcceleration(SystemConstants.acceleration);
			rightMotor.setAcceleration(SystemConstants.acceleration);
			driverStep=910;
		}
	}
	
}