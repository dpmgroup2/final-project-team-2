package BrickOne.Input;

import lejos.util.Timer;
import lejos.util.TimerListener;
import BrickCommunication.B1toB2Connection;
import BrickOne.InputProcessor.InputProcessor;
import BrickOne.PrimaryController.RobotController;
import Settings.Protocol;
import Settings.SystemConstants;

/**
 * Holds all the input controllers, will loop thru the controllers and store the data in DataCollected 
 * @author Bassam Riman
 *
 */
public final class Input implements TimerListener{
	//Link to Up class
	private InputProcessor inputProcessor;
		
	//Number of time InputProcess has processed
	//private int fetchCount;
	
	//Holds all InputControllers
	private IInputController[] InputsHolder;
	
	//Holds all collected data
	private int[][] dataCollected = new int[4][3];
	
	/**
	 * Class Constructor
	 * @param inputProcessor
	 */
	public Input(InputProcessor inputProcessor, RobotController robo, B1toB2Connection comm) {
		
		//Add IInput Here ***************************************************************
		IInputController LightSensorController = new LightSensorController();
		IInputController TachoCountSensorController = new TachoCountSensorController();
		IInputController UltrasonicSensorController = new UltrasonicSensorController(comm);
		
		this.InputsHolder = new IInputController[3];
		this.InputsHolder[Protocol.LightSensorsDATA]=LightSensorController;
		this.InputsHolder[Protocol.TachoCountSensorsDATA]=TachoCountSensorController;
		this.InputsHolder[Protocol.UltrasonicSensorDATA]=UltrasonicSensorController;	
		//********************************************************************************
		
		this.inputProcessor = inputProcessor;
		Timer inputTimer= new Timer(SystemConstants.InputPERIOD, this);
		robo.addTimer(inputTimer, this);
	}

	/**
	 * Implementation Of TimeListener Timeout
	 */
	@Override
	public void timedOut() {
		((UltrasonicSensorController)this.InputsHolder[Protocol.UltrasonicSensorDATA]).askData();
		
		
		for (int i=0; i<InputsHolder.length; i++){
			this.dataCollected[i]=this.InputsHolder[i].getData();
			
		}
		dataCollected[Protocol.TouchSensorDATA][Protocol.fTouchS]=
				(int)((UltrasonicSensorController)InputsHolder[Protocol.UltrasonicSensorDATA]).getTouch();
		
		this.inputProcessor.startInputProcessing(dataCollected);
		
		
	}
}