package BrickOne.Input;

import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import BrickCommunication.B1toB2Connection;
import BrickOne.PrimaryController.RobotController;


/**
 * Controls the ultrasonic sensors
 * @author Bassam Riman
 *
 */
public class UltrasonicSensorController implements IInputController{
	private static B1toB2Connection comm;
	private static UltrasonicSensor frontSonic = new UltrasonicSensor(SensorPort.S3);
	private static boolean usEnabled=true;
	
	private final int FILTER_OUT = 5, FILTER_THRESH=15;
	private static int[] frontSonicA = new int[4];
	private byte touch=0;
	

	public UltrasonicSensorController(B1toB2Connection comm){
		this.comm=comm;
		for(int i=0; i<3; i++) frontSonicA[i]=0;
		frontSonicA[3]=500;
		frontSonic.continuous();
	}
	
	/**
	 * Implements getData() in IInputController
	 */
	public int[] getData() {
		int[] temp = {70,70,70};
		if(RobotController.btDone) {
			temp = comm.getSlaveDATA();
			touch = (byte)temp[0];
		}
		if(usEnabled) {
			filterData();
			temp[0]=frontSonicA[2];
		}
		return temp;
	}
	
	public byte getTouch(){
		return touch;
	}
	
	public void askData() {
		if (RobotController.btDone) {
			comm.askSlaveDATA();
		}
	}
	
	public static void disableUS(){
		usEnabled=false;
		comm.disableUS();
		frontSonic.off();
	}
	
	public static void enableUS(){
		usEnabled=true;
		comm.enableUS();
		for(int i=0; i<3; i++) frontSonicA[i]=0;
		frontSonicA[3]=500;
		frontSonic.continuous();
	}
	
	private void filterData(){
		//Poll the sensor and get the distance
		int distance = frontSonic.getDistance();
		if (distance > 70 && frontSonicA[0] < FILTER_OUT) {
			//If it's an error and the error limit is not met,
			//asume there's a gap and tell the controller to go straight
			//and increment the counter
			frontSonicA[0] ++;
		}
		else if (distance>70){
			//Once the error limit is reached, consider this as a true 255 
			frontSonicA[2]=70;
			frontSonicA[1]=0;
		}
		else {
			//The sensor has picked up the wall once again, reset filter
			frontSonicA[0] = 0;
			if( (Math.abs(distance-frontSonicA[2])>FILTER_THRESH || Math.abs(distance-frontSonicA[3])>FILTER_THRESH )  && frontSonicA[1] <FILTER_OUT) {
				frontSonicA[3]=distance;
				frontSonicA[1]++;
				
			}
			else {
				frontSonicA[2]=distance;
				frontSonicA[3]=500;
				frontSonicA[1]=0;
			}
		}
	}
}