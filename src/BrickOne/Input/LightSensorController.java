package BrickOne.Input;

import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

/**
 * Controls the light sensors
 * @author Bassam Riman
 *
 */
public class LightSensorController implements IInputController {
	private int[] oldData = new int[2];
	private int[] var = new int[2];
	private int[] triggered = new int [2];
	
	LightSensor Leftlight = new LightSensor(SensorPort.S1);
	LightSensor Rightlight = new LightSensor(SensorPort.S2);
	/**
	 * Implements getData() in IInputController
	 */
	@Override
	public int[] getData() {
		//add additional data collector for additional sensors here
		int[] temp = new int[2];
		temp[0] = Leftlight.getNormalizedLightValue();
		temp[1] = Rightlight.getNormalizedLightValue();
				
		return filterData(temp);

	}

    private int[] filterData(int[] data) {
    	int[] out = {0,0};
    	
    	for(int i=0; i<2; i++) {
    		int j=oldData[i]-data[i];
    		if( j>-3 && triggered[i]==0) {
        		var[i]+=j;
        		if(var[i]>50) {
        			triggered[i]=1;
        			out[i]=1;
        		}
        		else if(var[i]<0) var[i]=0;
    		}
    		else {
    			var[i]=0;
    			triggered[i]=0;
    		}
			oldData[i]=data[i];
    	}
    	return out;
    }
}