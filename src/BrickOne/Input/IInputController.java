package BrickOne.Input;
/**
 * Interface that all Input Controller should implement
 * @author Bassam Riman
 *
 */
public interface IInputController {
	/**
	 * gets data regard of which sensor we are getting data from
	 * @return
	 */
	public int[] getData();
}
