package BrickOne.Input;
import lejos.nxt.Motor;

/**
 * Controls tachocount 
 * @author Bassam Riman
 *
 */

public class TachoCountSensorController implements IInputController {
	
	/**
	 * Implements getData() in IInputController
	 */
	@Override
	public int[] getData () {

		//add additional data collector for additional sensors here	
		int[] temp = new int[2];
		temp[0] = Motor.B.getTachoCount();
		temp[1] = Motor.A.getTachoCount();
		return temp;
	}
}