package BrickOne.InputProcessor;

import BrickCommunication.B1toB2Connection;
import BrickOne.Input.Input;
import BrickOne.PrimaryController.RobotController;
import Settings.Protocol;


/**
 * Holds all Input processing classes, will loop thru the inputProcess and store the processed data in ProcessData
 * @author Bassam Riman
 *
 */
public class InputProcessor {
	private RobotController robo;
	private IInputProcess[] InputProcessHolder;
	public Input input;

	//The processed data are stored here
	private float[][] ProcessedData = new float[3][6];
	
	public InputProcessor(RobotController robotController, B1toB2Connection comm){
		
		
		this.robo=robotController;
		
		//Add IIinputProcess Here *******************************************************
		IInputProcess odometerInputProcess = new OdometerInputProcess();
		IInputProcess localizationInputProcess = new LocalizationInputProcess();
		IInputProcess driverInputProcess = new DriverInputProcess();
		
		this.InputProcessHolder = new IInputProcess[3];
		this.InputProcessHolder[Protocol.odometerInputProcess]=odometerInputProcess;
		this.InputProcessHolder[Protocol.localizationInputProcess]=localizationInputProcess;
		this.InputProcessHolder[Protocol.driverInputProcess]=driverInputProcess;
	
		//*******************************************************************************
		
		
		input=new Input(this, robotController, comm);
	}
	
	/**
	 * Implementation Of TimeListener Timeout
	 * @param dataCollected 
	 */
	public void startInputProcessing(int[][] dataCollected) {
		
		for (int i=0; i<this.InputProcessHolder.length;i++){
			this.ProcessedData[i]=this.InputProcessHolder[i].processInput(dataCollected);
		}
		
		this.robo.run(ProcessedData);
	}
	
	public IInputProcess[] getInputProcessHolder() {
		return InputProcessHolder;
	}
}