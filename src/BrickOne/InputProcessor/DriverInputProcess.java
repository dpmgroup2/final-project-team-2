package BrickOne.InputProcessor;

import Settings.Protocol;

public class DriverInputProcess implements IInputProcess{

	
	@Override
	public float[] processInput(int[][] dataCollected) {
		float[] temp = {dataCollected[Protocol.UltrasonicSensorDATA][Protocol.rsUltrasonicS],
				dataCollected[Protocol.UltrasonicSensorDATA][Protocol.lsUltrasonicS],
				dataCollected[Protocol.UltrasonicSensorDATA][Protocol.fUltrasonicS],
				dataCollected[Protocol.LightSensorsDATA][Protocol.rLightS],
				dataCollected[Protocol.LightSensorsDATA][Protocol.lLightS],
				dataCollected[Protocol.TouchSensorDATA][Protocol.fTouchS],
				};
		return temp;
	}
}
