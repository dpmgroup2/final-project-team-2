package BrickOne.InputProcessor;

import Settings.Protocol;


/**
 * Handles input aspect of localization
 * @author Bassam Riman
 *
 */
public class LocalizationInputProcess implements IInputProcess{
	  /**
	   * Process data
	   */
	  @Override
	public float[] processInput(int[][] DataCollected) {
		  float[] temp= {DataCollected[Protocol.UltrasonicSensorDATA][Protocol.fUltrasonicS],
				  DataCollected[Protocol.LightSensorsDATA][Protocol.rLightS]};
		  return temp;
	  }
}