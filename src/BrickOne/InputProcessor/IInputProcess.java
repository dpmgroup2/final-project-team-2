package BrickOne.InputProcessor;



public interface IInputProcess{
	
	  /**
	   * Process data
	   */
	  public float[] processInput(int[][] dataCollected);
}
