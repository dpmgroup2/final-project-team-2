package BrickOne.InputProcessor;

import Settings.Protocol;
import Settings.SystemConstants;

/**
 * Handles robot positioning on the floor using tachocounts
 * @author Bassam Riman
 *
 */
public class OdometerInputProcess implements IInputProcess{
	// robot position
	private float x, y, theta, thetaRad;

	// current and previous Tacho values
	private float tachoLeft=0, tachoRight=0, prevTachoLeft=0, prevTachoRight=0, dTachoLeft=0, dTachoRight=0;
	
	/**
	 * Class Constructor
	 */
	public OdometerInputProcess(){
		x = 0;
		y = 0;
		theta = 0;
	}

	/**
	 * Process data
	 */
	@Override
	public float[] processInput(int[][] dataCollected) {

		tachoLeft= dataCollected[Protocol.TachoCountSensorsDATA][Protocol.lWheel];
		tachoRight= dataCollected[Protocol.TachoCountSensorsDATA][Protocol.rWheel];
		
		// compute the tachometer difference compared to last read, and store this one
		dTachoLeft=tachoLeft-prevTachoLeft;
		prevTachoLeft=tachoLeft;
		dTachoRight=tachoRight-prevTachoRight;
		prevTachoRight=tachoRight;
		
		// compute the robot's position according to newly read tacho values
		theta+=(dTachoLeft*SystemConstants.lWheel-dTachoRight*SystemConstants.rWheel)/SystemConstants.width;
		if(theta<0) theta+=360;
		if(theta>360) theta-=360;
		thetaRad=(float)Math.PI*theta/180;
		x+=Math.PI*Math.sin(thetaRad)*(dTachoRight*SystemConstants.rWheel+dTachoLeft*SystemConstants.lWheel)/360;
		y+=Math.PI*Math.cos(thetaRad)*(dTachoRight*SystemConstants.rWheel+dTachoLeft*SystemConstants.lWheel)/360;
		
		float[] temp= {x, y, theta};
		return temp;
	}
	
	// mutators
	public void setPosition(float[] position) {
		// ensure that the values don't change while the odometer is running
			x = position[0];
			y = position[1];
			theta = position[2];
	}

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}

	public void setTheta(float theta) {
		this.theta = theta;
	}
}