package BrickCommunication;

public final class BrickComProtocol {
	/**
	 * Class that contains the protocol of communication between master brick and slave
	 */
	//Fire
	public static final byte fire=-9;
	public static final byte fireReady=-10;
	public static final byte fireDone=-11;
	public static final byte fireNotDone=-12;
	public static final byte getSlaveData=-2;
	public static final byte enableUS=-7;
	public static final byte disableUS=-8;
	public static final byte deployDef=-13;
}
