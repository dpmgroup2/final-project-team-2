package BrickCommunication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.comm.NXTCommConnector;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.RS485;
import BrickTwo.CatapultController;
import BrickTwo.SlaveSensorController;
import BrickTwo.Bluetooth.BluetoothConnection;
import BrickTwo.Bluetooth.Transmission;
/**
 * Allows Brick to brick communication, this code is used for second brick
 */
public class B2toB1Connection extends Thread {
	NXTCommConnector connector = RS485.getConnector();
	int mode = NXTConnection.PACKET;
	SlaveSensorController ssC;
	CatapultController cc= new CatapultController();
	public static boolean ioExc=false;
	BluetoothConnection bt;
	Transmission trans;
	
	public B2toB1Connection() {
		bt=new BluetoothConnection();
		trans=bt.getTransmission();
		ssC=new SlaveSensorController();	
		cc.start();
	}
	
	
	/**
	 * Method that wait for command from the master and initiate a response or action
	 */
	@Override
	public void run(){
		while (true)
		{
			LCD.drawString("Waiting...", 0, 2);
			
			NXTConnection con = connector.waitForConnection(0, mode);
			ioExc=false;
			LCD.drawString("Connected...", 0, 2);
			
			
			DataInputStream dis = con.openDataInputStream();
			DataOutputStream dos = con.openDataOutputStream();
			
			try {
				dos.writeByte((byte)trans.role.getId());
				dos.writeByte((byte)trans.bx);
				dos.writeByte((byte)trans.by);
				dos.writeByte((byte)trans.w1);
				dos.writeByte((byte)trans.w2);
				dos.writeByte((byte)trans.d1);
				dos.writeByte((byte)trans.startingCorner.getId());
				dos.flush();
			}
			catch (IOException e){ioExc=true;}
			
			while(!ioExc) {
				int n=0;
				try{n = dis.readByte();} catch (IOException e) {ioExc=true;}
				
				switch(n) {
					case BrickComProtocol.deployDef:
						cc.deployDef();
						break;
					case BrickComProtocol.disableUS:
						ssC.disableUS();
						break;
					case BrickComProtocol.enableUS:
						ssC.enableUS();
						break;
					case BrickComProtocol.getSlaveData:
						try {
							dos.writeByte(ssC.getTouch());
							dos.writeByte(ssC.getLSonicDistance());
							dos.writeByte(ssC.getRSonicDistance());
							dos.flush();
						} catch (IOException e) {ioExc=true;}
						break;
										
					case BrickComProtocol.fireReady:
						if(cc.getShoot()) try {
							dos.writeByte(BrickComProtocol.fireNotDone);
							dos.flush();} catch (IOException e) {ioExc=true;}
						else try {
							dos.writeByte(BrickComProtocol.fireDone);
							dos.flush();} catch (IOException e) {ioExc=true;}
						break;
						
						
					case BrickComProtocol.fire:
						int x=0;
						try{x = dis.readByte();} catch (IOException e) {ioExc=true;}
						cc.setDistance(x);
						cc.setShoot(true);
						break;
						
					default:break;
				}
			}
		}
	}
}


