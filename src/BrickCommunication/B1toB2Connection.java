package BrickCommunication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.comm.NXTCommConnector;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.RS485;
/**
 * Allows Brick to brick communication, this code is used for main brick
 */
public class B1toB2Connection {
	int mode=NXTConnection.PACKET;
	NXTCommConnector connector = RS485.getConnector();
	String name = "Skarner";
	DataInputStream dis;
	DataOutputStream dos;
	
	/**
	 * Class Constructor
	 */
	public B1toB2Connection(){
	}
	
	public void connect(){
		NXTConnection con=null;
		while(con==null) {
			con = connector.connect(name, mode);
		}
		dis = con.openDataInputStream();
		dos = con.openDataOutputStream();
	}
	
	/**
	 * Command to fire
	 * @param numberOfTimes
	 */
	
	public void close(){
		try {
			dis.close();
		} catch (IOException e) {
		}
		try {
			dos.close();
		} catch (IOException e) {
		}
	}
	public void fire(int tiles){
		sendThis(BrickComProtocol.fire);
		sendThis((byte) tiles);
	}

	public void disableUS(){
		sendThis(BrickComProtocol.disableUS);
	}
	
	public void enableUS(){
		sendThis(BrickComProtocol.enableUS);
	}
	
	public boolean fireDone() {
		sendThis(BrickComProtocol.fireReady);
		if(getThis(1)[0]==BrickComProtocol.fireDone) return true;
		else return false;
	}
	
	public void deployDef(){
		sendThis(BrickComProtocol.deployDef);
	}
	
	/**
	 * Command to get ultrasonic data
	 * @param numberOfTimes
	 */
	public int[] getSlaveDATA(){
		int[] response = getThis(3);
		return response;
	}
	
	public void askSlaveDATA(){
		sendThis(BrickComProtocol.getSlaveData);
	}
	
	/**
	 * Method that allow the communication, this method is called by all commands
	 * @param command
	 * @param needResponse
	 * @param numberOfResponceExpected
	 * @return
	 */
	public void sendThis(byte command){
		try{
			dos.writeByte(command);
			dos.flush();
		}
		catch (IOException ioe)
		{
		}

	}
	
	public int[] getThis(int numberOfBytes) {
		int[] response = new int[numberOfBytes];
		for (int i = 0; i < numberOfBytes; i++) {
			try {
				response[i]=dis.readByte();
			} catch (IOException ioe) {
			}
		}
		return response;
	}
}
