package tests;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.nxt.comm.NXTCommConnector;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.RS485;

/**
 * Sending data between bricks
 * @author Jacob Barnett
 *
 */

public class CommTestSlave {

	public void main(String[] args) throws Exception {
		
		NXTCommConnector connector = RS485.getConnector();
		int mode = NXTConnection.PACKET;
		
		LCD.drawString("Initializing", 0, 0);
		
		NXTConnection conn = connector.waitForConnection(10000, mode);
		
		DataInputStream dis = conn.openDataInputStream();
		DataOutputStream dos = conn.openDataOutputStream();
		
		char data = '0';
		
		/*
		try {
			data = dis.readChar();
			LCD.drawString("Read: " + data, 0, 1);
			dos.writeChar(data);
			dos.flush();
			
			dis.close();
			dos.close();
			conn.close();
		}
		catch (IOException ioe){
			Sound.buzz();
			LCD.drawString("Error", 0, 1);
			Thread.sleep(2000);
			System.exit(1);
		}
		*/
		
		try {
			data = dis.readChar();
			LCD.drawString("Read: " + data, 0, 1);
		}
		catch (IOException ioe) {
			Sound.buzz();
			LCD.drawString("Write error", 0, 3);
			Thread.sleep(2000);
			System.exit(1);
		}
		
		try {
			dos.writeChar(data);
			dos.flush();
		}
		catch (IOException ioe) {
			Sound.buzz();
			LCD.drawString("Read error", 0, 4);
			Thread.sleep(2000);
			System.exit(1);
		}
		
		try {
			LCD.drawString("Closing connections", 0, 4);
			dis.close();
			dos.close();
			conn.close();
		}
		catch (IOException ioe) {
			LCD.drawString("Close error", 0, 5);
			Thread.sleep(2000);
			System.exit(1);
		}
	}
}
