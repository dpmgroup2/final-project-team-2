package tests;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.nxt.comm.NXTCommConnector;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.RS485;

/**
 * Sending data between bricks
 * @author Jacob Barnett
 *
 */

public class CommTestMaster {

	public static void main(String[] args) throws Exception {
		
		String name = "00165312C975"; // address of scorpion
		NXTCommConnector connector = RS485.getConnector();
		int mode = NXTConnection.PACKET;
		
		LCD.clear();
		LCD.drawString("Ready to connect to " + name, 0, 0);
		
		NXTConnection conn = connector.connect(name, mode);
		
		if (conn == null) {
			LCD.drawString("Connection failed", 0, 1);
			Thread.sleep(2000);
			System.exit(1);
		}
		
		LCD.drawString("Connected", 0, 1);
		
		DataOutputStream dos = conn.openDataOutputStream();
		DataInputStream dis = conn.openDataInputStream();
		
		try {
			dos.writeChar('A');
			dos.flush();
			LCD.drawString("Sent: A", 0, 2);
		}
		catch (IOException ioe) {
			Sound.buzz();
			LCD.drawString("Write error", 0, 3);
			Thread.sleep(2000);
			System.exit(1);
		}
		
		try {
			LCD.drawString("Confirmed:" + dis.readChar(), 0, 3);
		}
		catch (IOException ioe) {
			Sound.buzz();
			LCD.drawString("Read error", 0, 4);
			Thread.sleep(2000);
			System.exit(1);
		}
		
		try {
			LCD.drawString("Closing connections", 0, 4);
			dis.close();
			dos.close();
			conn.close();
		}
		catch (IOException ioe) {
			LCD.drawString("Close error", 0, 5);
			Thread.sleep(2000);
			System.exit(1);
		}
	}
}
