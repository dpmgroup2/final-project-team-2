package tests;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.USB;
import lejos.util.Delay;
import lejos.util.LogColumn;
import lejos.util.NXTDataLogger;

/**
 * Pulls data from the sensors and feeds it into the NXT Charter Logger
 * @author Jacob Barnett
 *
 */
public class SensorTest {
	public static void main(String[] args) {
		int buttonChoice;
		Object sensor = new Object();
		do {
			// clear the display
			LCD.clear();

			// ask the user whether the motors should drive in a square or float
			LCD.drawString("< Left | Right >", 0, 0);
			LCD.drawString("       |        ", 0, 1);
			LCD.drawString("    LS | US  ", 0, 2);
			buttonChoice = Button.waitForAnyPress();
		} while (buttonChoice != Button.ID_LEFT
				&& buttonChoice != Button.ID_RIGHT &&  buttonChoice != Button.ID_ESCAPE);

		if(buttonChoice == Button.ID_ESCAPE){
			System.exit(0);
		}else if (buttonChoice == Button.ID_LEFT) {
			sensor =  new LightSensor(SensorPort.S1);
		} else {
			sensor = new UltrasonicSensor(SensorPort.S2);
		}
		startTest(sensor);
		System.exit(0);
		
	}

	private static void startTest(Object sensor) {
		//FileOutputStream output;
		//IOException fileNotFound= new IOException();
		//LightSensor lightSensor= new LightSensor(SensorPort.S1);
		LogColumn someNumber;
		if(sensor instanceof LightSensor){
			someNumber= new LogColumn("LightSensor 1", LogColumn.DT_INTEGER);
		}else{
			someNumber= new LogColumn("UltrasonicSensor 1", LogColumn.DT_INTEGER);
		}
	
		LogColumn[] columnsDefs = new LogColumn[]{someNumber};
		NXTDataLogger logger= new NXTDataLogger();
		LCD.clear();
		LCD.drawString("Launch nxjchartinglogger ", 0, 0);
		NXTConnection connection= USB.waitForConnection();
		
		try {
			logger.startRealtimeLog(connection);
		} catch (IOException e) {
			System.exit(0);
		}
		logger.setColumns(columnsDefs);
		
		while(!Button.ESCAPE.isDown()){
			LCD.clear();
			if(sensor instanceof LightSensor){
				logger.writeLog( ((LightSensor) sensor).getNormalizedLightValue());
				LCD.drawString("LightSensor ", 1, 1 );
				LCD.drawString(" " + ((LightSensor) sensor).getNormalizedLightValue(), 2, 2);
			}else{
				logger.writeLog( ((UltrasonicSensor) sensor).getDistance());
				LCD.drawString("UltrasonicSensor ", 1, 1 );
				LCD.drawString(" " + ((UltrasonicSensor) sensor).getDistance(), 2, 2);
			}
			LCD.drawString(" Press Escape", 0, 5);
			LCD.drawString("  to Stop... ", 0, 6);
			logger.finishLine();
			
			//interval
			Delay.msDelay(500);
		}
		logger.stopLogging();
	}
}
