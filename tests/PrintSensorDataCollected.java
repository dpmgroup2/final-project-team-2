package BrickOne.RobotController;

import lejos.nxt.comm.RConsole;
import BrickOne.RobotController.Secondary.Protocol;

/**
 * Class used for testing, this class prints the collected data from the input sensor
 * @author Bassam Riman
 *
 */
public class PrintSensorDataCollected {
	public static void printSensorDataCollected(int[][]dataCollected ){
//		RConsole.println("[Light Sensors]: [Right]=" + dataCollected[Protocol.LightSensorsDATA][Protocol.rLightS]+
//			" [Left]=" + dataCollected[Protocol.LightSensorsDATA][Protocol.lLightS]);
//		RConsole.println("[Tacho Counts]: [Right]=" + dataCollected[Protocol.TachoCountSensorsDATA][Protocol.rWheel]+
//			" [Left]=" + dataCollected[Protocol.TachoCountSensorsDATA][Protocol.lWheel]);
//		RConsole.println("[Touch Sensor]: " + dataCollected[Protocol.TouchSensorDATA][Protocol.fTouchS]);
		RConsole.println("[Ultrasonic Sensors]:" /*+ " [LSide]="+dataCollected[Protocol.UltrasonicSensorDATA][Protocol.lsUltrasonicS]*/ + " [Front]= " + dataCollected[Protocol.UltrasonicSensorDATA][Protocol.fUltrasonicS]/* + " [RSide]=" + dataCollected[Protocol.UltrasonicSensorDATA][Protocol.rsUltrasonicS]*/);
	}
}
