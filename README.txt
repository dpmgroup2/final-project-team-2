DPM Group 2 Final Project

Members:
	Jacob Barnett
	Bassam Riman
	Stefan Ipate
	Justin Asfour
	Diego Kent-Haulard
	Haimonti Das
	
Profesors:
	Frank P. Ferrie
	David A. Lowther
	
Meetings:
	08:50am Mondays: With the professor(s)
	12:30pm Wednesdays: Team meeting
	
Current branch:
	master